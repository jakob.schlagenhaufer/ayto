###############################
# Are You The One - Germany
# Season: X
###############################

###############################
# List of the group1' names
# Important: Only 10 names are allowed
group1 = ["XXX",    #0
        "XXX",      #1
        "XXX",      #2
        "XXX",      #3 
        "XXX",      #4
        "XXX",      #5
        "XXX",      #6
        "XXX",      #7
        "XXX",      #8
        "XXX"]      #9

###############################
# List of the group2' names
# Important: Only 11 names are allowed
group2 = ["XXX",    #0
         "XXX",     #1
         "XXX",     #2
         "XXX",     #3
         "XXX",     #4
         "XXX",     #5
         "XXX",     #6
         "XXX",     #7
         "XXX",     #8
         "XXX",     #9
         "XXX"]     #10

###############################
# Each truth booth revelation
# [group1_member_index, group2_member_index, perfect_match?]
truth_booth = [[1,0,False],
               [0,3,False],
               [8,8,False],
               [8,8,False],
               [3,3,False],
               [2,0,False],
               [7,5,True], 
               [9,8,False],
               [2,2,False],
               [5,7,False]]

###############################
# Each matching ceremony guess and number of resultant beams
# Matchup explaination: 
#   Key:    Index of group 2 
#   Value:  Index of group 1
# [[group2_member_0_group1_member, group2_member_1_group1_member, ... group2_member_8_group1_member, group2_member_9_group1_member], number_correct]
mc = [[[0,1,2,3,4,5,6,7,8,9,-1],3],
       [[4,1,2,3,7,9,8,5,6,0,-1],2],
       [[9,1,3,2,4,5,6,7,8,0,-1],2],
       [[7,1,4,3,2,5,6,8,9,0,-1],3],
       [[0,8,3,2,-1,9,6,5,7,1,4],2],
       [[0,4,2,-1,8,7,3,5,6,1,9],4], 
       [[0,1,4,9,2,7,3,5,6,-1,8],6],
       [[0,4,2,9,8,7,3,5,6,1,-1],4],
       [[0,4,-1,9,6,7,5,3,2,1,8],2],
       [[0,1,4,2,5,7,3,8,6,9,-1],10]]
