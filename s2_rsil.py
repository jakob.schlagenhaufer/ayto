###############################
# Are You The One - Reality Stars In Love - Germany
# Season: 2
###############################

###############################
# List of the group1' names
# Important: Only 10 names are allowed
group1 = ["Franziska", #0
        "Anna",     #1
        "Celina",   #2
        "Ricarda",  #3 
        "Gina",     #4
        "Cecilia",  #5
        "Karina",   #6
        "Isabelle", #7
        "Luisa",    #8
        "Zoe"]      #9

###############################
# List of the group2' names
# Important: Only 11 names are allowed
group2 = ["Max",    #0
         "Micha",   #1
         "Martin",  #2
         "Maurice", #3
         "Calvin",  #4
         "Amadu",   #5
         "Pharrell",#6
         "Lukas",   #7
         "Fabio",   #8
         "Luca",    #9
         "Felix"]   #10

###############################
# Each truth booth revelation
# [group1_member_index, group2_member_index, perfect_match?]
truth_booth = [[9,2,False],
               [6,6,False],
               [2,7,False],
               [8,7,True], # Gina sold, but Lukas left so perfect match revealed
               [3,10,False],
               [7,6,False],
               [5,5,True],
               [1,1,True]]

###############################
# Each matching ceremony guess and number of resultant beams
# Matchup explaination: 
#   Key:    Index of group 2 
#   Value:  Index of group 1
# [[group2_member_0_group1_member, group2_member_1_group1_member, ... group2_member_8_group1_member, group2_member_9_group1_member], number_correct]
mc = [[[0,1,2,3,4,5,6,7,8,9,-1],3],
       [[2,1,7,3,8,4,5,9,6,0,-1],2],
       [[9,4,1,3,6,5,8,0,2,7,-1],3],
       [[9,4,1,3,6,5,8,0,2,7,-1],3], # Duplicated because cancled
       [[3,0,6,7,-1,5,1,8,2,9,4],3],
       [[0,1,7,5,6,-1,3,8,2,9,4],3],  
       [[0,1,7,-1,6,5,3,8,2,9,4],4],  
       [[-1,1,6,9,0,5,3,8,4,2,7],5]  
       ]

