###############################
# Are You The One - Germany
# Season: 3
###############################

###############################
# List of the group1' names
# Important: Only 10 names are allowed
group1 = ["Leon",   #0
        "Mike",     #1
        "Max",      #2
        "Antonio",  #3 
        "Tim",      #4
        "Dustin",   #5
        "William",  #6
        "Marius",   #7
        "Andre",    #8
        "Jordi"]    #9

###############################
# List of the group2' names
# Important: Only 11 names are allowed
group2 = ["Jessica",#0
         "Joelina", #1
         "Kerstin", #2
         "Monami",  #3
         "Marie",   #4
         "Zaira",   #5
         "Dana",    #6
         "Isabelle",#7
         "Raphaela",#8
         "Estelle", #9
         "Desiree"] #10

###############################
# Each truth booth revelation
# [group1_member_index, group2_member_index, perfect_match?]
truth_booth = [[1,0,False],
               [0,3,False],
               [8,8,False],
               [8,8,False],
               [3,3,False],
               [2,0,False],
               [7,5,True], 
               [9,8,False],
               [2,2,False],
               [5,7,False]]

###############################
# Each matching ceremony guess and number of resultant beams
# Matchup explaination: 
#   Key:    Index of group 2 
#   Value:  Index of group 1
# [[group2_member_0_group1_member, group2_member_1_group1_member, ... group2_member_8_group1_member, group2_member_9_group1_member], number_correct]
mc = [[[0,1,2,3,4,5,6,7,8,9,-1],3],
       [[4,1,2,3,7,9,8,5,6,0,-1],2],
       [[9,1,3,2,4,5,6,7,8,0,-1],2],
       [[7,1,4,3,2,5,6,8,9,0,-1],3],
       [[0,8,3,2,-1,9,6,5,7,1,4],2],
       [[0,4,2,-1,8,7,3,5,6,1,9],4], 
       [[0,1,4,9,2,7,3,5,6,-1,8],6],
       [[0,4,2,9,8,7,3,5,6,1,-1],4],
       [[0,4,-1,9,6,7,5,3,2,1,8],2],
       [[0,1,4,2,5,7,3,8,6,9,-1],10]]
