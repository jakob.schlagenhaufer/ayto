import importlib
import sys
import pandas as pd
from itertools import permutations
from os import listdir
from os.path import isfile, join

##################
# Load correct season based on argument
#
try:
    season = sys.argv[1]
except IndexError:
    raise IndexError("No season argument.\nPlease supply a season as an argument, e.g. ./ayto-script.py s3")
try:
    season_data = importlib.import_module(season)
except ImportError:
    seasons = [f for f in listdir(".") if isfile(join(".",f)) and f[-3:] == ".py" and join(".",f) != sys.argv[0]]
    seasons = ", ".join([f[:-3] for f in seasons]) + "."
    raise ImportError("There is no season: {}.\nTry one of the following: {}".format(season,seasons))
group1 = season_data.group1
group2 = season_data.group2
truth_booth = season_data.truth_booth
mc = season_data.mc
length_group1 = len(group1)
length_group2 = len(group2)


##################
# Define functions
#
def num_shared(a,b):
    return sum([a[i] == b[i] for i in range(length_group1)])

def tally_pairings(remaining_guesses):
    temp = [[0]*length_group1 for r in range(length_group2)]
    for matchup in remaining_guesses:
        for group2_member, group1_member in enumerate(matchup):
            temp[group2_member][group1_member] += 1
    return temp

def printProbability(remaining_guesses, typeForPrintDesc):
    print("Calculating probabilities ... please wait ...")
    temp = tally_pairings(remaining_guesses)
    probability = pd.DataFrame(temp,columns=group1,index=group2)
    num_guesses = len(remaining_guesses)
    probability *= 100/num_guesses
    print("Probability %")
    print(probability)
    print("\nEpisode {}, {}\n{} guess(es) left\n".format(i+1,typeForPrintDesc,num_guesses))

##################
# Initialisation
#
print("Calculating all possible guesses")
remaining_guesses = list(permutations(range(0,length_group1)))
num_guesses = len(remaining_guesses)

print("{} guesses initial with 10x10\n".format(num_guesses))
print("Adding the 11th person to guesses")

##################
# Duplicating adding the 11th person to each guess
guesses_with_11 = []
for i in range(length_group1):
# for i in range(1):
    print("Add guesses for match: " + group2[10] + " + " + group1[i])
    for guess in remaining_guesses:
        guess_list = list(guess)
        guess_list.append(i)
        # guess.append = group1[i]
        guess = tuple(guess_list)
        guesses_with_11.append(guess)

remaining_guesses = guesses_with_11
num_guesses = len(remaining_guesses)
print(pd.DataFrame([[110.0/length_group2]*length_group2 for i in range(length_group1)],columns=group2,index=group1))
print("{} guess(es) left\n".format(num_guesses))
#
# End of initialisation
##################


##################
# main loop
for i in range(0,len(mc)): 
    
    ###############################
    # Truth booth
    # Eliminate all guesses that don't fit this episode's truth booth result
    remaining_guesses = [guess for guess in remaining_guesses if (guess[truth_booth[i][1]] == truth_booth[i][0]) == truth_booth[i][2]]
    printProbability(remaining_guesses,"truth booth")
    
    ################################
    # Match ceremony
    # Eliminate all guesses that don't fit this episode's match ceremony
    remaining_guesses = [guess for guess in remaining_guesses if num_shared(guess,mc[i][0]) == mc[i][1]]
    printProbability(remaining_guesses, "match ceremony")


