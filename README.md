# Calculator for "Are You The One" - TV show

## Summary

This calculator calculatest the odds of matches in each episode of a season. So you can see at which point in the season they could already know their perfect matches. It is tested with the german seasons you can see in the project already.

Note: This calculator is made for a "10x11" season. E.g. 10 men and 11 women or vice versa.

Credits to daturkel for inspiration on the coding. You can find his project here: https://github.com/daturkel/ayto-calculator

---

## Usage

Run the script `ayto-script.py` and append the name of the season. 

```
./ayto-script.py <season-file-name>
```

Example:
```
./ayto-script.py s3
```
---
## Add your own season

Just copy the template and fill it with you data of you season. Then execute the script and append the name of you season file. Keep in mind group2 always has to be the larger one.

---

## Special adjustment when using for 10x10 

If you want to use this calculator for a season with 10x10 setup. You have to do followin adjustments:
- give the 11th person of group2 a random name
- in all mc: give the 11th person match "-1" like so: [0,1,2,3,4,5,6,7,8,9,-1]
- ignore the person in the Table. 

Keep in mind: There will be 10 guesses in the end, but all of them have the first 10 persons identical.
